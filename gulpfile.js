var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat-css');
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();

gulp.task('serve', function() {
  browserSync.init({
    server: "./dist"
  });
  gulp.watch(["app/js/*.js"], ['js']);
  gulp.watch(["app/less/*.+(less|css)"], ['less']);
  gulp.watch(['dist/index.html']).on('change', browserSync.reload);
});

gulp.task('less', function () {
  return gulp.src('app/less/*.+(less|css)') 	
    .pipe(less()) 	
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('common.css'))         
    .pipe(minifyCSS())                 
    .pipe(rename('common.min.css'))     	       
    .pipe(gulp.dest('dist/style'))
    .pipe(browserSync.stream());
});

gulp.task('js', function () {
  gulp.src('app/js/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('dist/js'));
  //.pipe(browserSync.stream());
});

gulp.task('sprite', function () {
  var spriteData = gulp.src('app/images/sprite/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css'
  }));
  spriteData.img.pipe(gulp.dest('dist/images'));
  spriteData.css.pipe(gulp.dest('app/less'));
});

gulp.task('imagemin', function () {
  gulp.src('app/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images'))
});

gulp.task('default', ['imagemin', 'less', 'js', 'serve']);
